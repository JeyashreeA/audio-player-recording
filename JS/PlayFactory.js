/*global console:true,require:true,angular:true*/
var app = angular.module('myApp');

var ffi = require('ffi');           //To form a wrapper around the DLL

app.factory('playerService', function(){
var player = {};
    var audio_dll = ffi.Library('ConsoleApplication6', {
            'initialize': [ 'int', ['string']],
            'play':['int',[]],
            'pause':['int',[]],
            'stop':['int',[]] 
        });
    var timeFrame, i;
    player.initialize = function(file) {
        timeFrame = audio_dll.initialize(file);
        return timeFrame;
    }; 
    player.play = function() {
        audio_dll.play();
    };
    player.pause = function() {
        audio_dll.pause();
    };
    player.stop = function() {
        audio_dll.stop();
    }; 
    
    return player;
});

