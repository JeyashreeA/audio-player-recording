var app = angular.module('myApp');

    app.factory('chartService', function(){

        var chartFunctionalities = {};
        var chart = new CanvasJS.Chart("chart");
        
        var dataSeries1 = { 
                type: "spline",
                markerType : 'triangle',
                lineThickness: 2,
                color: "black"
            };
        
        chartFunctionalities.createChart = function() {
        
             
            chart.options = {
                zoomEnabled: true,
                animationEnabled: true,
                title: {
                    text: "Your voice waves :P"
                },
                axisX: {
                    labelAngle: 10,
                    valueFormatString: " ",
                    tickLength: 0,
                    title: "Frames Taken"
                },
                axisY: {
                    gridThickness: 0,
                    includeZero: false,
                    valueFormatString: " ",
                    tickLength: 0,
                    title: "Amplitude"
                }
            };
            chart.options.data = [];
            
        
        }

        chartFunctionalities.drawChart = function(dataPoints1){

            console.log("am being called");
            dataSeries1.dataPoints = dataPoints1;
            chart.options.data.push(dataSeries1);
            //Better to construct options first and then pass it as a parameter
            chart.render();
            //freeing up memory
            dataPoints1 = null;
            return true;
        }
//        
//        chartFunctionalities.destroyChart = function(){
//        
//            chart = null;
//        
//        }

        return chartFunctionalities;

    });