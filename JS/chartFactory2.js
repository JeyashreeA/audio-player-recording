var app = angular.module('myApp');

    app.factory('chartService', function(){

        var chartFunctionalities = {};
        chartFunctionalities.chart;
        chartFunctionalities.createChart = function(chartData) {
            chartFunctionalities.chart = AmCharts.makeChart("chartdiv", {
                "type": "serial",
                "theme": "light",
                "legend": {
                        "useGraphSettings": true
                },
                "dataProvider": chartData,
                "valueAxes": [{
                        "id":"v1",
                        "axisColor": "#FF6600",
                        "axisThickness": 2,
                        "position": "left"
                }, {
                        "id":"v2",
                        "axisColor": "#FCD202",
                        "axisThickness": 2,
                        "position": "right"
                }],
                "graphs": [{
                        "valueAxis": "v1",
                        "lineColor": "#FF6600",
                        "bullet": "round",
                        "hideBulletsCount": 30,
                        "title": "Left",
                        "valueField": "left",
                "fillAlphas": 0
                }, {
                        "valueAxis": "v2",
                        "lineColor": "#FCD202",
                        "bullet": "square",
                        "hideBulletsCount": 30,
                        "title": "Right",
                        "valueField": "right",
                "fillAlphas": 0
                }],
                "chartCursor": {
                        "cursorPosition": "mouse",
                    "selectWithoutZooming": true,
                    "pan": false
                },
                "categoryField": "xaxis",
                "categoryAxis": {
                    "gridPosition": "start",
                },
                "export": {
                    "enabled": true,
                    "libs": {
                        "path": "http://www.amcharts.com/lib/3/plugins/export/libs/"
                    },	
                        "menu": [ {
                        "class": "export-main",
                        "menu": [ "PNG", "JPG", "CSV" ]
                    } ]
                }
            });
            
        }
        
        
        
        chartFunctionalities.drawChart = function(){
        
            chartFunctionalities.chart.write("chart");
        
        }
        return chartFunctionalities;
    });
