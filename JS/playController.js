/*global console:true,require:true,angular:true,document:true,setTimeout:true,setInterval:true,clearTimeout:true,clearInterval:true*/

var app = angular.module('myApp');//Includes angular
var path = require('path');
var nw = require('nw.gui');
var win = new nw.Window.get();           //To create a Node webKit window.
var timer, i, timerChange, openFile;

var menuBar = new nw.Menu({            //Creates the menu bar
    type: 'menubar'
});

var menu = new nw.Menu();
menu.append(new nw.MenuItem({           //create an menu item

    label:'open',
    click: function() {
        openFile = document.getElementById('readFile');
        openFile.click();
    }

}));

menuBar.append(new nw.MenuItem({            //Append an item to the menu bar
    label:'File',
    submenu: menu
}));

nw.Window.get().menu = menuBar;


app.controller("myCtrl",['$scope','playerService', 'chartService', function($scope, playerService, chartService){
    console.log("am loaded");
    $scope.check1=false;
    $scope.files = [];
    $scope.filesPath = [];
    $scope.songDetails = 0;
    $scope.clicked = 0;
    $scope.changeTime = 0;
    $scope.openStream = false;
    $scope.playAs = {
    
        kind:"solo"
        
    };
    $scope.playerPlay = playerService;
    
    $scope.callPlay = function(){
        console.log("am being called")
        $scope.file = document.getElementById("readFile");
        if ($scope.file.multiple === true) {
            for (var i = 0, len = $scope.file.files.length; i < len; i++) {
                $scope.files.push($scope.file.files.item(i).name);
                $scope.filesPath.push($scope.file.files.item(i).path);
            }
        } 
        else {
            $scope.files.push($scope.file.files.item(0).name);
            $scope.filesPath.push($scope.file.files.item(0).path);
        }
        
        $scope.check=true;
        console.log("files"+$scope.files);
        console.log("filesPath"+$scope.filesPath);
        console.log("check"+$scope.check);
        $scope.$apply(function(){console.log("am running");});
//        for (var i = 0, len = $scope.file.files.length; i < len; i++) {
//                $scope.files.pop($scope.file.files.item(i).name);
//                $scope.filesPath.pop($scope.file.files.item(i).path);
//            } 
        $scope.files.push("file.wav");
        $scope.filesPath.push("D:\\Jeyashree\\Node-ffi\\Audio_player\\dynamic1\\Music\\file.wav");
        console.log("files"+$scope.files);
        console.log("filesPath"+$scope.filesPath);
        $scope.songDetails = $scope.playerPlay.initialize($scope.filesPath[0])+1;
        console.log($scope.songDetails);
        document.getElementById('slider').max = $scope.songDetails-1;
        document.getElementById('slider').value = 0;
    };
    
    $scope.removeSong = function(index){
    
        $scope.files.splice(index,1);
        $scope.filesPath.splice(index,1);
    
    };
    
    $scope.clickedRow = function(val) {
        var start = new Date().getTime();
        
        $scope.clicked = val;
        $scope.stop();
        $scope.songDetails = $scope.playerPlay.initialize($scope.filesPath[val])+1;
        document.getElementById('slider').max = $scope.songDetails-1;
        $scope.play();
        
        var end = new Date().getTime();
        var time = end-start;
        console.log("Execution Time"+time);
    };
    
    $scope.play = function(){        
        $scope.sliderSeek();
        $scope.continuePlay();
    };
    
    $scope.continuePlay = function() {
        $scope.playerPlay.play();
        console.log("song"+$scope.songDetails);
        timer = setTimeout(function(){
            $scope.clicked ++;
            if($scope.clicked === $scope.file.files.length || $scope.playAs.kind === "solo") {
                    console.log("if");
                $scope.clicked = 0;
                $scope.stop();
                }
            else if($scope.playAs.kind === "play") {
                console.log("else");
                $scope.stop();
                $scope.songDetails = $scope.playerPlay.initialize($scope.filesPath[$scope.clicked])+1;
                console.log("$scopetime"+$scope.songDetails);
                document.getElementById('slider').max = $scope.songDetails-1;
                $scope.sliderSeek();
                $scope.continuePlay();
            }
            
        }, $scope.songDetails*1000);
        console.log(timer);
    };
    
    $scope.sliderSeek = function(){
    
        timerChange = setInterval(function(time){
            
            $scope.changeTime++;
            document.getElementById("slider").value = $scope.changeTime;
            if($scope.changeTime === $scope.songDetails) {
                $scope.changeTime = 0;
                document.getElementById("slider").value = $scope.changeTime;
                clearInterval(timerChange);
            }
        
        },1000, $scope.songDetails);
    };
    
    $scope.pause = function(){
        
        $scope.playerPlay.pause();
        if(timer){
        clearTimeout(timer);
        }
        
        if(timerChange){
        clearInterval(timerChange);
        }
        
    };
    
    $scope.stop = function(){
        
        $scope.playerPlay.stop();
        if(timer){
            clearTimeout(timer);
        }
        
        if(timerChange){
            $scope.changeTime = 0;
            document.getElementById("slider").value = $scope.changeTime;
            clearInterval(timerChange);
        }
        $scope.songDetails = $scope.playerPlay.initialize($scope.filesPath[$scope.clicked])+1;
        document.getElementById('slider').max = $scope.songDetails-1;
    };
    
}]); 


