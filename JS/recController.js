var app = angular.module('myApp');//Includes angular
var fs = require('fs');
app.controller("myRecCtrl",['$scope','recordService','$http', function($scope, recordService, $http){
    var json ;
    $scope.recorder = recordService;
    console.log($scope.recorder);
    $scope.startOrStop = false;

    
     
        
    fs.readFile("audio.txt", 'utf8', function (err, data) {
        if (err){ console.log(err); }
        
        else {
            
            console.log(typeof data); 
            json = JSON.parse("[" + data + "]");       //converting the data into an array of JS arrays(this is how the inputs were given in)
    //        json = JSON.parse(data);       //had tried working it out with array of JS objects
            console.log(json);
            console.log(json.length);
               $scope.chartConfig = {
                    options: {                        /*chart config options*/
                        chart: {
                            type:'line',
                            zoomType: 'x'

                        },
                        title: {
                            text: 'USD to EUR exchange rate over time'
                        },
                        subtitle: {
                            text: document.ontouchstart === undefined ?
                                    'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
                        },
                        xAxis: {
                            type: 'category',
                            allowDecimals: false
                        },
                        yAxis: {
                            title: {
                                text: 'Exchange rate'
                            }
                        },
                        legend: {
                            enabled: false
                        }
                    },
                        series: [{
                            type: 'line',
                            name: 'USD to EUR',
                            data: json                              /*data to be given*/
                            }]
        
                    }
            }
        
    });
    var processed_json = new Array();                   //Jquery way
//var json1 = {"0":"0",
//"1":"65535",
//"2":"0",
//"3":"-65536",
//"4":"0",
//"5":"0",
//"6":"0",
//"7":"0",
//"8":"0",
//"9":"0",
//"10":"0",
//"11":"65535",
//"12":"0",
//"13":"0",
//"14":"0",
//"15":"0",
//"16":"5832930",
//"17":"-13369788",
//"18":"-286794502",
//"19":"-13762853",
//"20":"4128701",
//"21":"-15073641",
//"22":"-3473482",
//"23":"-4391287",
//"24":"2817981",
//"25":"-7471478",
//"26":"-12452174",
//"27":"-25362902",
//"28":"-24117670",
//"29":"-17301988",
//"30":"-19136948",
//"31":"-25362847",
//"32":"-10617165",
//"33":"-13762838",
//"34":"-11993291",
//"35":"-18678039",
//"36":"-7930094",
//"37":"-5701801",
//"38":"-11993236",
//"39":"-8847659",
//"40":"-5636271",
//"41":"-6619319",
//"42":"-5308667",
//"43":"-8323288",
//"44":"-10158304",
//"45":"-9699477",
//"46":"-17367277",
//"47":"-10092774",
//"48":"-16449801",
//"49":"-14155999",
//"50":"-14221611"}
    
//    $.getJSON("audio_data2.txt", function(json1){    
//         console.log("hi");
//        
//            for(var key in json1){
//            
//                processed_json.push([key, parseInt(json1[key])]);
//            
//            }
//            console.log(processed_json);
        
//        $('#container').highcharts({
//				chart: {
//					type: 'line'
//				},
//				xAxis: {
//					type: "category"
//				},
//				series: [{
//					data: processed_json
//				}]
//			});
//        })

    
   $scope.click = function() {                      //the chart value is assigned to the chart once this function is invoked. Even though I assign the value after the value is processed it still works this way.
   
       console.log($scope.chartConfig.series.data)
   
   }
    
    $scope.toggle = function() {
    
        (!$scope.startOrStop)?($scope.startOrStop=true):($scope.startOrStop=false);
    
    };
    
    $scope.record = function() {
    
        if($scope.startOrStop) {
            $scope.recorder.initialize();
            alert("you may start recording");
            $scope.recorder.startRec();
        }
        
        else {
        
            $scope.time = $scope.recorder.stopRec();
            console.log($scope.time);
        
        }

    };
    

    
//  (function () {
                // generate an array of random data
//                var data = [];
//                    var i,j,temparray,chunk = 1000;
//                    for (i=0,j=json.length; i<j; i+=chunk) {
//                        
//                    temparray = json.slice(i,i+chunk);
//                    data.push.apply(
//                        temparray
//                    );
//                }
//                return data;
//            }())  
//            