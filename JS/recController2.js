var app = angular.module('myApp');//Includes angular
var fs = require('fs');
var q = require('q');


app.controller("myRecCtrl",['$scope', '$interval', 'recordService', 'chartService', function($scope, $interval, recordService, chartService){
console.log("am loaded");
    $scope.recorder = recordService;
    $scope.charter = chartService;
    
    //variable to set the record process
    $scope.startOrStop = false;
    
    //performing panning
    $scope.showLeftPan=false;
    $scope.showRightPan=false;
    var center = 0, centerDiff = 0;
    
    //Boolean variable to check for left and right channels
    $scope.startingRight = false;
    
    //varaibles for setting the min and max values within file and range
    $scope.maxVal = 0;
    $scope.minVal = 0;
    $scope.maxValObj = {};
    $scope.minValObj = {};
    $scope.minValArr = [];
    $scope.maxValArr = [];
    var minLabel = 0, maxLabel = 0;
    
    //variable to keep track of files written
    $scope.noOfFiles = 0;
    var fileName;
    
    //variable to set the location of the mini and max value in the array of objects
    var checkForMaxMinLocation;
    var startFile = 0, endFile = 0, k;
    
    //setting the $interval variable
    var stop;
    var stopRepeat;
    
    //creating the initial string array containing the data of the file
    var json = [];
    
    
    //Using promises--> making the async function return a promise
    var readFileP = q.denodeify(fs.readFile);
    
    //Creation of the initial chart
    $scope.createChart = function() {
        
        $scope.charter.createChart($scope.dataPoints1);
        console.log("inside createchart" + $scope.dataPoints1.length);
        //generation of graph
        $scope.charter.drawChart();
        //Attaching listener for zooming functionality
        $scope.dataPoints1 = [];
        $scope.charter.chart.chartCursor.addListener("selected", $scope.handleZoomAndPan);
    
    };

    //Handling zoom and panning functionalities
    $scope.handleZoomAndPan = function(event) {
        
        //Invoked when used for the zoom--> provides the starting and ending indices and calls appropriate functions.

        if(typeof event === "object") {
            
            $scope.start = $scope.panStart = ($scope.charter.chart.dataProvider[event.start].location)*2;
            $scope.end = ($scope.charter.chart.dataProvider[event.end].location)*2;
            $scope.createDataForZoom();
            if(!$scope.showLeftPan || !$scope.showRightPan) {
                $scope.showLeftPan = true;
                $scope.showRightPan = true;
                $scope.$digest();
            
            }
            
        }
        //generation of data in case of zoom out.
        else if(event === "clicked") {
//            $scope.createData(filesZoom);
        }
        
        
    };
    
    //function to toggle the functionality of the record button
    $scope.toggle = function() {
    
        (!$scope.startOrStop)?($scope.startOrStop=true):($scope.startOrStop=false);
    
    };
    
    //generation of the array of objects for input to the chart
    
    function getData (start, end, fileNumber, event) {
//        end = end || 88200;
        var range = end - start;
        var incrementVal = 0;
        
        var intermediate1 = {};
        var updatedMin = false;
        var updatedMax = false;
        //setting the increment value for generating the data
        if(range <=5) {

            $scope.charter.chart.chartCursor.selectWithoutZooming = false;
            $scope.charter.chart.validateNow();
            return;
        
        }
        
        if(range < 200) {
        
            incrementVal = 1;
        
        }
        
        else { 
        
            incrementVal = parseInt(range / 200);
        
        }
        
        console.log(incrementVal);
        if(event && start%2 === 0) {
        
            $scope.startingRight = true;
        
        }
        
        for (var i=start; i < end; i+=incrementVal)
            {
                //inserting the min and max values
                var k = i+1;
                checkForMaxMinLocation = i - incrementVal;
                    if(checkForMaxMinLocation < minLabel  && i > incrementVal && !updatedMin) {
                        intermediate1 = $scope.minValArr[fileNumber];
                        updatedMin = true;
                    
                    }
                    else if(checkForMaxMinLocation < maxLabel && i > incrementVal && !updatedMax) {
                    
                        intermediate1 = $scope.maxValArr[fileNumber];
                        console.log("file "+fileNumber+"obj");
                        console.log(intermediate1);
                        updatedMax = true;
                    
                    }
                else {
                //performing in case of zooming
                    if(event) {
                        intermediate1.xaxis = Number(((parseInt(i/2))/(44100)).toFixed(6));
                        intermediate1.location = parseInt(i/2);
                        if($scope.startingRight) {
                            intermediate1.right = Number(json[(i)-(fileNumber*88200)]);
                            intermediate1.left = Number(json[(k)-(fileNumber*88200)]);
                        }
                        else {
                            intermediate1.left = Number(json[(i)-(fileNumber*88200)]);
                            intermediate1.right = Number(json[(k)-(fileNumber*88200)]);
                        }
                    }
                    else if(!event) {
                            intermediate1.xaxis = fileNumber+Number((parseInt(i/2)/(44100*(fileNumber+1))).toFixed(6));
                            intermediate1.location = parseInt(i/2)+((fileNumber)*44100);
                            intermediate1.left = Number(json[i]);
                            intermediate1.right = Number(json[k]);
                    }
                }
                
                $scope.dataPoints1.push(intermediate1);
                intermediate1 = {};
            }
        console.log("am done--getData");
  }
    
    //finding the min and max values within the file and range
    $scope.findingMinAndMax = function(start, end, fileNumber, event) {
        
        if(event) {
            start = (start/(fileNumber+1));
            end = (end/(fileNumber+1));
        }
        
        $scope.maxVal = json[start];
        $scope.minVal = json[start];
    
        for(var j = start; j < end; j++) {
            
            if(json[j] < $scope.minVal) {
            
                    $scope.minVal = json[j];
                    minLabel = j;
                }
            
            if (json[j] > $scope.maxVal) {
            
                    $scope.maxVal = json[j];
                    maxLabel = j;
            }
        }
        
        $scope.minValObj = {
                        xaxis : fileNumber+Number((parseInt(minLabel/2)/(44100*(fileNumber+1))).toFixed(6)),
                        location : parseInt(minLabel/2)+((fileNumber)*44100),
                        left : Number(json[minLabel]),
                        right : Number(json[minLabel+1])
                        };
        
        $scope.maxValObj = {
                        xaxis : fileNumber+Number((parseInt(maxLabel/2)/(44100*(fileNumber+1))).toFixed(6)),
                        location : parseInt(maxLabel/2)+((fileNumber)*44100),
                        left : Number(json[maxLabel]),
                        right : Number(json[maxLabel+1])
                        };
        $scope.maxValArr.push($scope.maxValObj);
        $scope.minValArr.push($scope.minValObj);
        console.log("am done--min-max");
    };
    
    //creating the data for the initial plotting of chart
    $scope.createData = function (filesZoom) {
        $scope.dataPoints1 = [];
        var files = [];
        
        //initial gragh data generation
        endFile = $scope.noOfFiles-1;

        //creating an array of promises

        fileName = './recorded/file'+startFile+'.txt';        
        files.push(readFileP(fileName, 'utf-8'));

        //executed when all promises are resolved
        q.all(files)
        .then(function(content){
            
            
                $scope.dataPoints1 = [];
                json = content[0].split(/\n/);
                json.pop();
                
                //data generation for initial functionality
                $scope.findingMinAndMax(0, json.length, startFile);
                getData(0, json.length, startFile);
                if(startFile === 0) {

                    $scope.createChart();

                }
                else {

                    $scope.charter.chart.dataProvider = $scope.charter.chart.dataProvider.concat($scope.dataPoints1);
                    $scope.charter.chart.validateData();

                }
                startFile++;
            
            //nulifying the json data
            json = null;
            
        })
        .fail(function(e){          //Handling failure to read files
            console.log(e);
            console.log('No dice');
        });
    };
    
    
    
    $scope.createDataForZoom = function() {
    
        $scope.dataPoints1 = [];
        var files = [];
        
        //Using promises--> making the async function return a promise
        var readFileP = q.denodeify(fs.readFile);
        
        //Conditions to be set for zooming
        startFile = parseInt(($scope.start)/88200);
        endFile = parseInt(($scope.end)/88200);
        console.log("startfile :"+ startFile+"endFile:" +endFile);
        for(var k=startFile; k<=endFile; k++) {
                fileName = './recorded/file'+k+'.txt';        
                files.push(readFileP(fileName, 'utf-8'));
            }
        
        q.all(files)
        .then(function(content){
            
            for(var i=0;i<files.length;i++){
                json = content[i].split(/\n/);
                json.pop();
                
                //Data generation for zooming functionality
                $scope.endIntermediate = $scope.end;
                if(files.length > 1) {

                    if(endFile === startFile) {
                        $scope.endIntermediate = $scope.end;
                    }
                    else {
                        startFile === 0?$scope.endIntermediate = 88200:$scope.endIntermediate = (startFile+1)*88200;
                    }


                }
                console.log("$scope.start"+ $scope.start);
                console.log("$scope.endInt "+ $scope.endIntermediate);
                $scope.findingMinAndMax($scope.start, $scope.endIntermediate, startFile, "zoom");
                getData($scope.start, $scope.endIntermediate, startFile, "zoom");
                $scope.start = $scope.endIntermediate;
                startFile++;
            }
            
            //Updating the chart data in case of zoom
            $scope.charter.chart.dataProvider = $scope.dataPoints1;
            $scope.charter.chart.validateData();

            //nulifying the json data
            json = null;
            
        })
        .fail(function(e){          //Handling failure to read files
            console.log(e);
            console.log('No dice');
        });
    };
    
    
    $scope.record = function() {
        
        if($scope.startOrStop) {
            
            //starting the recorder
            
             if($scope.charter.chart) {
            
                //deallocating chart data
                $scope.charter.chart.dataProvider = [];
                $scope.charter.chart.validateData();
                $scope.charter.chart.removeListener(chart, "selected", $scope.handleZoomAndPan);
                $scope.noOfFiles = 0;
                startFile = 0;
                $scope.charter.chart = null;
            }
           
            $scope.recorder.initialize();
            alert("you may start recording");
            $scope.recorder.startRec();
            
            stop = $interval(function() {
                
                $scope.noOfFiles++;       
                $scope.createData();

            },1000);
        }
        
        else {

            $scope.frames = $scope.recorder.stopRec();
            stopRepeat = $interval(function (){
                
                if (angular.isDefined(stop)) {
                    $interval.cancel(stop);
                    stop = undefined;
                }
                $interval.cancel(stopRepeat);
            },1000);
        }

    };

}]);