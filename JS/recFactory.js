/*global console:true,require:true,angular:true*/
var app = angular.module('myApp');

var ffi = require('ffi');           //To form a wrapper around the DLL

app.factory('recordService', function(){
var recorder = {};
    var audio_dll = ffi.Library('ConsoleApplication10', {
            'initialize': [ 'int', []],
            'startRec':['int',[]],
            'stopRec':['int',[]]
        });
    var timeFrame, i;
    recorder.initialize = function(file) {
        audio_dll.initialize();
    }; 
    recorder.startRec = function() {
        audio_dll.startRec();
    };
    recorder.stopRec = function() {
        timeFrame = audio_dll.stopRec();
        return timeFrame;
    }; 
    
    return recorder;
});


