/*global angular:true*/
var app = angular.module('myApp', ['ngRoute']);

app.config(["$routeProvider", function($routeProvider){

    $routeProvider
    
    .when('/',{
    
        templateUrl:'HTML/playBack.html',
        controller : 'myCtrl'
    
    })
    .when('/record',{
    
        templateUrl:'HTML/record.html',
        controller : 'myRecCtrl'
    
    });

}]);