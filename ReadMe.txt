The contents of this repo consist of a node webkit assignment - "Audio Player". 

To run the application, click on nw.exe application. 

The first page is for the playback purposes. The related files can be found in the folders JS, HTML, CSS, Images, Music.


The second page in the application has the functionality of a recorder. The file recController1.js is the file containing the controller for the working chart. 

recController.js is the file that has controller of the previously used chart library (highcharts).

Folders D3 and C3 are for the second chart library (c3js) that I was using. Chart Trials is where I was just doing a trial and error examples.